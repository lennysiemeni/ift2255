package runners;

import java.time.LocalDateTime;
import java.util.ArrayList;

import beans.Client;
import beans.Service;
import crud.ClientCrud;
import crud.InscriptionCrud;
import crud.Persistance;
import crud.ServiceCrud;
import system_comms.Validation;

/**
 * Handles creating a TEF file, must be it's own class because it is a Runnable object (must be Runnable to be scheduled)

 */
public class TefWeeklyCreation implements Runnable{

	/**
	 * Creates the TEF file
	 */
	@Override
	public void run() 
	{
		ArrayList<Client> clients = ClientCrud.reader(Validation.CLIENTSFILE);
		ArrayList<Service> servs = ServiceCrud.reader(Validation.SERVICEFILE);
		ArrayList<String[]> rowList = InscriptionCrud.reader(Validation.INSCRIPTIONFILE);
		
		ArrayList<String> als = new ArrayList<String>();
		String s = "";
		int ctr = 0;
		
		for(int i=0; i < clients.size(); i++)
		{
			s = "";
			ctr = 0;
			
			if(clients.get(i).isPro())
			{
				s += clients.get(i).getNom() + " - ";
				s += clients.get(i).getId() + " - ";
				
				//Check if there are any services under the pro's name
				for(int j=0; j < servs.size(); j++)
				{
					//If any service belongs to this professional
					if(servs.get(j).getCodeDePro().equals(clients.get(i).getId()))
					{
						s += servs.get(j).getName();
						
						//If anyone has ever registered for that class we will count them
						for(int x = 0; x < rowList.size(); x++)
						{
							//serviceCode @ [0] - clientId @ [1]
							if(rowList.get(x)[0].equals(servs.get(j).getName()))
							{
								ctr++;
							}
						}
						s += " - Nombre de personne inscrit: " + ctr;
					}
				}
				
				
			}
			als.add(s);
		}
		
		int len = LocalDateTime.now().toString().length();
		String filename = LocalDateTime.now().toString().substring(0, len-4);
		filename = filename.replaceAll(":", "-");
		
		Persistance.createTef(filename, als);
		Acco.comptabiliter(filename);
	}

}
