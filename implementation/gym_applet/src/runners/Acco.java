package runners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import beans.Service;
import crud.ServiceCrud;
import crud.TefRead;
import system_comms.Validation;

/**
 * Handles the accounting process
 *
 */
public class Acco 
{

	/**
	 * Reads a TEF file and generates a report 
	 * @param filename TEF filename
	 */
	@SuppressWarnings("rawtypes")
	public static void comptabiliter(String filename) 
	{
		//Chaque element de la liste est un String de format: Wael - 0157 - CrossFit - Nombre de personne inscrit: 5
		ArrayList<String> als = TefRead.readTefFile(filename);
		
		ArrayList<Service> servicesList = ServiceCrud.reader(Validation.SERVICEFILE);
		
		//Hashmap qui a le nom du pro en temps que cle et une liste de strings qui represente le nom de tous les services que ils offre
		HashMap<String, ArrayList<String>> services = new HashMap<String, ArrayList<String>>();
		
		//Hashmap pour contait l'argent que on doit au pro
		HashMap<String, Double> fees = new HashMap<String, Double>();
		
		//On loop tous le fichier
		for(int i = 0; i < als.size(); i++)
		{
			
			//Notre string de format: Wael - 0157 - CrossFit - Nombre de personne inscrit: 5
			String[] s = als.get(i).split("-");
			
			//Si le service exist mais il ya aucun memebre inscrit, la valeur aurrait le format de Wael - 0157 - CrossFit
			//Donc on skip de rentrer cette information
			if(s.length == 3)
			{
				continue;
			}
			
			//On cree une nouvelle araylist pour contenir les object
			ArrayList<String> hold = new ArrayList<String>();
			
			String trimmedName = s[0];
			trimmedName = trimmedName.trim();
			String trimmedService = s[2];
			trimmedService = trimmedService.trim();
			
			//On check si on a deja ce pro dans notre hashmap, si le cas on vas ajouter un autre service a ca list
			if(services.containsKey(trimmedName))
			{
				hold = services.get(trimmedName);
				hold.add(trimmedService);
				services.put(trimmedService, hold);
			}
			else
			{
				hold.add(trimmedService);
				services.put(trimmedName, hold);
			}
			
			for(int j = 0; j < servicesList.size() ; j++)
			{
				if(servicesList.get(j).getName().equals(trimmedService))
				{
					if(fees.containsKey(trimmedName))
					{
						fees.put(trimmedName, fees.get(trimmedName) + servicesList.get(j).getFrais());
					}
					else
					{
						fees.put(trimmedName, servicesList.get(j).getFrais());
					}
				}
			}
		}
		
		//On a une lsite de tous les pro a payer, tous les service de chaque pro
		System.out.println("Tous les pro a payer + leur services offerd: ");
	    Iterator it = services.entrySet().iterator();
	    int ctrService = 0;
	    
	    double fraisPourProTotal = 0;
	    while (it.hasNext()) 
	    {
	        Map.Entry pair = (Map.Entry)it.next();
	        System.out.print(pair.getKey() + " : ");
	        ArrayList<String> servDePro = services.get(pair.getKey());
	        ctrService = 0;
	        for(String s: servDePro)
	        {
	        	ctrService++;
	        	System.out.print(s);
	        }
	        
	        System.out.println("Nombre de service par ce pro: " + ctrService);
	        System.out.println("Nombre de frais a payer pour ce pro: " + fees.get(pair.getKey()));
	        fraisPourProTotal += fees.get(pair.getKey());
	        it.remove(); // avoids a ConcurrentModificationException
	    }
	    
	    System.out.println("Le nombre total de pro: " + services.keySet().size());
	    System.out.println("Le nombre total de frais a payer: " + fraisPourProTotal);
	    
	}
	
}
