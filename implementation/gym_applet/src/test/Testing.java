package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.Test;

import beans.Client;
import beans.Service;
import crud.ClientCrud;
import crud.CrudChoice;
import crud.InscriptionCrud;
import crud.Persistance;
import crud.ServiceCrud;
import runners.TefWeeklyCreation;
import system_comms.Validation;

public class Testing {

	/**
	 * Testing Validation.checkClientNumber
	 */
	@Test
	public void testClientNumber()
	{
		Client c = new Client("Tester", "Prenom", "cell", "email", "address", false, false);
		Validation.writeClient(c, CrudChoice.ADD);
		int id = Integer.parseInt(c.getId());
		int result = Validation.checkClientNumber(id);
		
		assertEquals(result, 1);
	}
	
	@Test
	public void testEmail()
	{
		String email = "";
		String result = Validation.emailValidator(email);
		
		assertEquals(result, "Client existe pas");
	}
	
	@Test
	public void testClientCreate()
	{
		ArrayList<Client> clients = ClientCrud.reader(Validation.CLIENTSFILE);
		int beforeCreateExtra = clients.size();
		Client c = new Client("Tester", "Prenom", "cell", "email", "address", false, false);
		Validation.writeClient(c, CrudChoice.ADD);
		clients = ClientCrud.reader(Validation.CLIENTSFILE);
		int afterCreateExtra = clients.size();
		assertNotSame(beforeCreateExtra, afterCreateExtra);
	}
	
	@Test
	public void testServiceCreate()
	{
		ArrayList<Service> servs = ServiceCrud.reader(Validation.SERVICEFILE);
		int beforeCreateExtra = servs.size();
		
		Client c = new Client("Tester", "Prenom", "cell", "email", "address", true, false);
		ArrayList<DayOfWeek> dow = new ArrayList<DayOfWeek>();
		dow.add(DayOfWeek.MONDAY);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat formatter = new SimpleDateFormat("HH:mm");
		Date deb = null;
		Date fin = null;
		Time timeValue = null;
		try {
			deb = sdf.parse("11/11/2001");
			fin = sdf.parse("11/11/2011");
			timeValue = new Time(formatter.parse("13:30").getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Service serv = new Service("TestServ", LocalDateTime.now(), deb, fin, timeValue, dow, 10, c.getId(), 100, "");
		Validation.writeService(serv, CrudChoice.ADD);

		servs = ServiceCrud.reader(Validation.SERVICEFILE);
		int afterCreateExtra = servs.size();
		assertNotSame(beforeCreateExtra, afterCreateExtra);
	}
	
	@Test
	public void testTefCreation()
	{
		TefWeeklyCreation tef = new TefWeeklyCreation();
		tef.run();
	}
	
	@Test
	public void testInscriptionCreate()
	{
		ArrayList<String[]> inscrs = InscriptionCrud.reader(Validation.INSCRIPTIONFILE);
		int before = inscrs.size();
		
		ArrayList<Service> servs = ServiceCrud.reader(Validation.SERVICEFILE);
		Service serv = null;
		Client c = null;
		if(servs.get(0)==null)
		{
			c = new Client("Tester", "Prenom", "cell", "email", "address", true, false);
			ArrayList<DayOfWeek> dow = new ArrayList<DayOfWeek>();
			dow.add(DayOfWeek.MONDAY);
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			DateFormat formatter = new SimpleDateFormat("HH:mm");
			Date deb = null;
			Date fin = null;
			Time timeValue = null;
			try {
				deb = sdf.parse("11/11/2001");
				fin = sdf.parse("11/11/2011");
				timeValue = new Time(formatter.parse("13:30").getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}

			serv = new Service("TestServ", LocalDateTime.now(), deb, fin, timeValue, dow, 10, c.getId(), 100, "");
			Validation.writeService(serv, CrudChoice.ADD);
		}
		
		else
		{
			serv = servs.get(0);
			c = ClientCrud.reader(Validation.CLIENTSFILE).get(0);
		}
		
		Persistance.writeInscription(Validation.INSCRIPTIONFILE, CrudChoice.ADD, serv, c.getId());
		inscrs = InscriptionCrud.reader(Validation.INSCRIPTIONFILE);
		int after = inscrs.size();
		assertNotSame(before, after);
	}
	
}
