package system_comms;

import java.util.ArrayList;

import beans.Client;
import beans.Presence;
import beans.Service;
import crud.ClientCrud;
import crud.CrudChoice;
import crud.InscriptionCrud;
import crud.Persistance;
import crud.ServiceCrud;

/**
 * Class that contains methods that validate data
 */
public class Validation {

	//Le nom des fichier a acceder est hardcoder pour centralization
	public static final String CLIENTSFILE = "clients.xml";
	public static final String SERVICEFILE = "services.xml";
	public static final String PRESENCEFILE = "presence.xml";
	public static final String INSCRIPTIONFILE = "insc.xml";
	
	/**
	 * Validates whether a client number really exists or not
	 * @param result Client code
	 * @return boolean Is the client code valid or not
	 */
	public static int checkClientNumber(int result) {
		String id = ""+result;
		
		ArrayList<Client> clients = ClientCrud.reader(CLIENTSFILE);
		
		for(int i=0;i<clients.size();i++)
		{
			if(clients.get(i).getId().equals(id))
			{
				if(clients.get(i).isSuspendu() == false)
				{
					//Si on retourne 1 ca veut dire que on a trouver le membre et ils est valid
					return 1;
				}
				else
				{
					//Si on retourne -1 ca veut dire que le membre exist mais ils est suspendu
					return -1;
				}
			}
		}
		//Si on retourne -2 ca veut dire le membre exist pas
		return -2;
		
	}
	
	/**
	 * Writes to the client file
	 * @param c Client object
	 * @param choice Enum choice - what to do with the client Object
	 */
	public static void writeClient(Client c, CrudChoice choice)
	{
		Persistance.writeClient(CLIENTSFILE, c, choice);
	}
	
	/**
	 * Writes to the Service file
	 * @param c Service object
	 * @param choice Enum choice - what to do with the client Object
	 */
	public static void writeService(Service c, CrudChoice choice)
	{
		Persistance.writeService(SERVICEFILE, c, choice);
	}
	
	/**
	 * Method that returns a list of all Service's
	 * @return ArrayList<Service> ArrayList of all Service's
	 */
	public static ArrayList<Service> getAllServices()
	{
		return ServiceCrud.reader(SERVICEFILE);
	}
	
	/**
	 * Method that returns a list of all Pro's
	 * @return ArrayList<Service> ArrayList of all Pro's
	 */
	public static ArrayList<Client> getAllPros()
	{
		ArrayList<Client> onlyPros = new ArrayList<Client>();
		ArrayList<Client> allClients = ClientCrud.reader(CLIENTSFILE);
		
		
		for(int i=0;i<allClients.size();i++)
		{
			if(allClients.get(i).isPro())
			{
				onlyPros.add(allClients.get(i));
			}
		}
		
		return onlyPros;
	}
	
	/**
	 * Method that confirms the presence of a Client to a session
	 * @param p Presence object
	 * @param choice Enum choice - what to do with the client Object
	 */
	public static void confirmPresence(Presence p, CrudChoice choice)
	{
		Persistance.writePresence(PRESENCEFILE, p, choice);
	}
	
	/**
	 * Register a client for a service
	 * @param serv Service to register to
	 * @param clientId Client id of the client to register
	 */
	public static void registerForService(Service serv, String clientId)
	{
		Persistance.writeInscription(INSCRIPTIONFILE, CrudChoice.ADD, serv, clientId);
	}
	
	/**
	 * Validates a client by their email
	 * 
	 * @param email Client email
	 * @return String detailing the client status
	 */
	public static String emailValidator(String email)
	{
		ArrayList<Client> clients = ClientCrud.reader(CLIENTSFILE);
		
		for(int i=0;i<clients.size();i++)
		{
			if(clients.get(i).getEmail().equals(email))
			{
				//Valid
				if(!clients.get(i).isSuspendu())
				{
					System.out.println("Valid");
					return clients.get(i).getNom() + "+" + clients.get(i).getId();
				}
				else
				{
					return "Suspendu";
				}
			}
		}
		
		return "Client existe pas";
	}
	
	/**
	 * Method that handles generating bar-codes 
	 * @param c Client who's bar-code to generate
	 */
	public static void barcode(Client c)
	{
		/*
		Linear barcode = new Linear();
		barcode.setType(Linear.CODE39);
		
		//barcode data to encode
		barcode.setData(c.getId());
		 
		// wide bar width vs narrow bar width ratio
		barcode.setN(3.0f);
		 
		try {
			barcode.renderBarcode("c://code39.gif");
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
	}
	
	/**
	 * Method that returns ALL the Inscriptions 
	 */
	public static void tousLesInscription()
	{
		ArrayList<String[]> tousLesInsc = InscriptionCrud.reader(Validation.INSCRIPTIONFILE);
		for(int i=0; i < tousLesInsc.size(); i++)
		{
			System.out.println(tousLesInsc.get(i));
		}
	}
}
