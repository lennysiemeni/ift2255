package system_comms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import beans.Client;
import beans.Presence;
import beans.Service;
import crud.CrudChoice;
import runners.RnbSchedule;
import runners.TefWeeklyCreation;

/**
 * Main class that handles User Input and most main functionality
 */
public class Reception {

	/* Liste de String values que ont utilise dans notre programme */
	
	public static final String CONVOSTART = "Bonjour, que desirez vous faire? \n"
				+ "1-Donner l'accès à un membre \n"
				+ "2-Consulter le repertoire de service \n"
				+ "3-Enregistrer un membre pour un service \n"
				+ "4-Enregistrer un nouveau client \n"
				+ "5-Creer un nouveau service \n"
				+ "6-Consulter l'enregistrement pour une seance \n"
				+ "7-Confirmer la presence pour une seance \n"
				+ "8-Quitter le systeme \n"
				+ "9-Produire rapport comptable";

	public static final String CONVOUSERENTER = "Entrer le numero du membre";

	public static final String CONVOUSERCREATE = "Entrer le Nom, Prenom, Numero de Cell, address mail, address postal et un boolean pour indiquer si c'est un professionnel separer par des +";

	public static final String CONVOPROENTER = "Entrer le numero du professionnel";

	public static final String CONVOSERVICECREATE = "Entrer le nom, date de debut (dd/mm/yyyy), date de fin (dd/mm/yyyy), heure de service (HH:mm), "
			+ "occurence hebdomadaire (1234567 - chaque nombre fait reference a le jour de la semaine commencent a Lundi), " + 
			"maximum nombre de client, le coux du service, et des commentaire separer par des +";

	public static final String CONVOSERVICESIGNUP = "Entrer le code du service pour vous enregistrer: ";

	public static final String CONVOREGISTERBIEN = "Vous etes enregistrer!";
	
	public static final String CONVODBERROR = "Database integrity compromised, ont peut rien faire :(";

	public static final String CONVOINVALIDUSER = "Membre non valide";
	
	public static final String CONVOSUSPENDEDUSER = "Membre suspendu.";
	
	public static final String CONVOINVALIDINPUT = "Le format des donnees n'est pas valide, entrez 0 pour recommencer ou n'importe quelle autre touche pour retourner au menu principal";
	
	public static final String CONVOINVALIDOPTION = "Pas une option, entrez 0 pour recommencez ou n'importe quelle autre touche pour retourner au menu principale";
	
	public static final String CONVOUSERCREATED = "Voici votre numero d'utilisateur, gardez le bien: ";
	
	public static final String CONVOINVALIDOPTIONSTART = "Pas une option :(";
	
	public static final String CONVOSERVICEPRESENCE = "Entrer le code du service pour confirmer votre presence: ";
	
	public static final String CONVOPRESENCECONFIRM = "Merci pour confimer votre presence! Des commentaire? (entrer 0 sinon)";
	
	public static final String CONVOWELCOME = "Bienvenue!";
	
	public static final String CONVONOWELCOME = "Vous etes pas membre :(";
	
	public static final String CONVONOCASHWELCOME = "Vous avez des frais impayés naughty boy :'(";
	
	public static final String CONVOQUIT = "A la prochaine :D";
	
	public static final int INVALIDVALUE = -1;

	public static final int choices = 9;

	static Scanner sc = new Scanner(System.in);

	/**
	 * Main method to assure database and file integrity and start the program
	 * 
	 * @param args
	 */
	public static void main(String[] args) 
	{

		try 
		{
			assureFileIntegrity();
		} 
		catch (Exception e) 
		{
			System.out.println(CONVODBERROR);
			System.exit(-1);
		}
		
		startTimers();
		
		start();  
	}

	/**
	 * Method that starts up the scheduled timers
	 */
	private static void startTimers() 
	{
		//Prep and start RnbScheduler
		RnbSchedule rnb = new RnbSchedule();
		final ScheduledExecutorService sched = Executors.newScheduledThreadPool(2);
		
		int currentTime = LocalTime.now().getHour();
		int delay = 0;
		//On calcule un delay value pour le nombre d'heure d'ici 21h du jour meme
		if(currentTime < 21)
		{
			delay = 21-currentTime;
		}
		//Si par example c'est 23h, le delay va etre run apres un delay de 22h donc le jour prochain a 21h
		if(currentTime > 21)
		{
			delay = (24-currentTime)+21;
		}
		if(currentTime == 21)
		{
			delay = 0;
		}
		
		//On Schedule une tache qui, grasse au delay value va se run a 21 heure la premiere foi, et ce re-run apres 24h (donc les prochaine journee au meme temps)
		sched.scheduleAtFixedRate(rnb, delay, 24, TimeUnit.HOURS);
		
		
		
		//Prep and start TEF file creation timer
		TefWeeklyCreation tef = new TefWeeklyCreation();
		int curHour = LocalDateTime.now().getHour();
		//On calcule combien de temps ca nous prend pour arriver a la fin de la journee (en heure)
		int hourlyBigDelay = curHour-24;
		hourlyBigDelay = Math.abs(hourlyBigDelay);
		
		//On a deja pris en conte le temps que ca prend pour terminer la journee, on calcule base sur le 
		//fait que on est a minuit du jour meme, et on ajoute le nombre d'heure necessaire pour arriver a Vendredi
		switch(LocalDateTime.now().getDayOfWeek().getValue())
		{
			//Dimanche
			case 0:
				hourlyBigDelay += 24*5;
				break;
			//Dimanche	
			case 7:
				hourlyBigDelay += 24*5;
				break;
			//Lundi
			case 1:
				hourlyBigDelay += 24*4;
				break;
			//Mardi
			case 2:	
				hourlyBigDelay += 24*3;
				break;
			//Mercredi
			case 3:
				hourlyBigDelay += 24*2;
				break;
			//Jeudi
			case 4:
				hourlyBigDelay += 24*1;
				break;
			//Vendredi	
			case 5:	
				break;
			//Samedi
			case 6:
				hourlyBigDelay += 24*6;
				break;
		}
		
		
		
		
		sched.scheduleAtFixedRate(tef, hourlyBigDelay, 168, TimeUnit.HOURS);
		
	}

	/**
	 * Create the XML files that house our data if they do not already exist,
	 * assure basic XML elements are also present in the files after creation.
	 * 
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerException 
	 */
	public static void assureFileIntegrity() throws ParserConfigurationException, SAXException, IOException, TransformerException
	{
		File f1 = new File(Validation.CLIENTSFILE);
		if(!f1.exists()) 
		{ 
			PrintWriter writer = null;
			try {
				writer = new PrintWriter(Validation.CLIENTSFILE, "UTF-8");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			writer.close();
			
	        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	        Document doc = dBuilder.newDocument();
	        Element root = doc.createElement("Employees");
	        doc.appendChild(root);
	        doc.getDocumentElement().normalize();
	        TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(Validation.CLIENTSFILE));
			transformer.transform(source, result);
		}
		
		File f2 = new File(Validation.PRESENCEFILE);
		if(!f2.exists()) 
		{ 
			PrintWriter writer = null;
			try {
				writer = new PrintWriter(Validation.PRESENCEFILE, "UTF-8");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			writer.close();
			
	        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	        Document doc = dBuilder.newDocument();
	        Element root = doc.createElement("Presences");
	        doc.appendChild(root);
	        doc.getDocumentElement().normalize();
	        TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(Validation.PRESENCEFILE));
			transformer.transform(source, result);
		}
	
		File f3 = new File(Validation.SERVICEFILE);
		if(!f3.exists()) 
		{ 
			PrintWriter writer = null;
			try {
				writer = new PrintWriter(Validation.SERVICEFILE, "UTF-8");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			writer.close();
			
			
	        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	        Document doc = dBuilder.newDocument();
	        Element root = doc.createElement("Services");
	        doc.appendChild(root);
	        doc.getDocumentElement().normalize();
	        TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(Validation.SERVICEFILE));
			transformer.transform(source, result);
		}
		
		File f4 = new File(Validation.INSCRIPTIONFILE);
		if(!f4.exists()) 
		{ 
			PrintWriter writer = null;
			try {
				writer = new PrintWriter(Validation.INSCRIPTIONFILE, "UTF-8");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			writer.close();
			
	        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	        Document doc = dBuilder.newDocument();
	        Element root = doc.createElement("insc");
	        doc.appendChild(root);
	        doc.getDocumentElement().normalize();
	        TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(Validation.INSCRIPTIONFILE));
			transformer.transform(source, result);
		}
	}
	
	/**
	 * Method that manages the main menu and parses the user choices
	 */
	public static void start() 
	{
		System.out.println(CONVOSTART);
		String response = sc.next();
		int result = -1;

		try 
		{
			result = Integer.parseInt(response);


			if(result < 1 && result > choices)
			{
				throw new NumberFormatException();
			}
		}

		catch(NumberFormatException e)
		{
			System.out.println(CONVOINVALIDOPTIONSTART);
			start();
		}



		System.out.println(result);

		switch(result)
		{
			case 1:
				signIn();
				break;
			case 2:
				services();
				break;
			case 3:
				signInService();
				break;
			case 4:
				signUp();
				break;
			case 5:
				newService();
				break;
			case 6:
				consult();
				break;
			case 7:
				confirmPresence();
				break;
			case 8:
				System.exit(0);
				break;
			case 9:
			System.out.println("Generation du rapport comptable...");
			default:
				start();
				break;
		}
		
		start();
	}

	/**
	 * Method that deals with signing in a user, 
	 * accepts input and validates if it is a correct user number or not
	 */
	public static void signIn() 
	{
		System.out.println(CONVOUSERENTER);
		
		String response = sc.next();
		int result = intParse(response);

		int res = Validation.checkClientNumber(result);
		if(res == 1)
		{
			System.out.println(CONVOWELCOME);
		}
		else if(res == -1)
		{
			System.out.println(CONVONOCASHWELCOME);
		}
		else
		{
			System.out.println(CONVONOWELCOME);
		}
	}

	/**
	 * Method that prints out all services that exist
	 */
	public static void services() 
	{
		ArrayList<Service> servs = Validation.getAllServices();

		for(int i=0;i<servs.size();i++)
		{
			System.out.println(servs.get(i).getCodeDePro() + " - " + servs.get(i).getCodeService() + " - " + servs.get(i).getCommentaire() + " - " + servs.get(i).getMaxClients() + " - " + servs.get(i).getName() + " - " + servs.get(i).getDebut() + " - " + servs.get(i).getFin()
					+ " - " + servs.get(i).getHeurDeService() + " - " + servs.get(i).getLdtNow() + " - " + servs.get(i).getRecurrence());
		}
	}

	/**
	 * Method that signs in a user and registers them into a service.
	 * Once client number is confirmed will call signInUser() method.
	 * This separation is to prevent the user from re-entering their user number
	 * if they already entered a valid one, and does not have to repeat the sign in
	 * process from scratch if mistakes are made.
	 */
	public static void signInService()
	{
		System.out.println(CONVOUSERENTER);

		String response = sc.next();
		int result = intParse(response);

		int res = Validation.checkClientNumber(result);
		if(res == 1)
		{
			System.out.println(CONVOWELCOME);
			signInUser(response);
		}
		else
		{
			System.out.println(CONVONOCASHWELCOME);
			signInService();
		}

	}
	
	/**
	 * Returns all services (Their names and codes) and waits for the user to enter a code
	 * so that they may be singed up to the service
	 */
	private static void signInUser(String clientCode)
	{
		ArrayList<Service> servs = Validation.getAllServices();
		for(int i=0;i<servs.size();i++)
		{
			System.out.println(servs.get(i).getName() + " - " + servs.get(i).getCodeService());
		}

		System.out.println(CONVOSERVICESIGNUP);
		String signupservice = sc.next();
		boolean correctServiceCode = false;
		
		for(int i=0;i<servs.size();i++)
		{
			if(signupservice.equals(servs.get(i).getCodeService()))
			{
				System.out.println(CONVOREGISTERBIEN);
				for(int j=0;j<servs.size();j++)
				{
					if(servs.get(j).getCodeService().equals(signupservice))
					{
						Validation.registerForService(servs.get(j), clientCode);
					}
				}
				correctServiceCode = true;
			}
		}
		
		if(!correctServiceCode)
		{
			System.out.println(CONVOINVALIDINPUT);
			signupservice = sc.next();
			if(signupservice.equals("0"))
			{
				signInService();
			}
		}
	}


	/**
	 * Method that accepts user information to sign them up to the gym
	 */
	public static void signUp()
	{
		System.out.println(CONVOUSERCREATE);
		String response = sc.next();
		String[] data = response.split("\\+");
		
		if(data.length == 6)
		{
			boolean b1 = false;
			if(data[5].equals("true"))
				b1 = true;
			Client c = new Client(data[0], data[1], data[2], data[3], data[4], b1, false);
			Validation.writeClient(c, CrudChoice.ADD);
			System.out.println(CONVOUSERCREATED + c.getId());
		}
		
		else
		{
			System.out.println(CONVOINVALIDINPUT);
			response = sc.next();
			if(response.equals("0"))
				signUp();
		}
	}

	/**
	 * Method that accepts user input to create a new service.
	 * Method only accessible to pros.
	 * @return boolean If the service was successfully created
	 */
	public static boolean newService()
	{
		System.out.println(CONVOPROENTER);
		String response = sc.next();
		int result = intParse(response);

		ArrayList<Client> listdepro = Validation.getAllPros();
		boolean found = false;

		//Checks if the pro number is valid
		for(int i=0;i<listdepro.size();i++)
		{
			if(listdepro.get(i).getId().equals(""+result))
			{
				found = true;
				break;
			}
		}
		
		if(found)
		{
			System.out.println(CONVOSERVICECREATE);
			response = sc.next();
	
			String[] sesh = response.split("\\+");
	
			//Takes the input for the creation of a new service and assures validity
			if(sesh.length == 8)
			{
				try
				{
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					DateFormat formatter = new SimpleDateFormat("HH:mm");
					Date deb = null;
					Date fin = null;
					Time timeValue = null;
					int maxi = 0;
					ArrayList<DayOfWeek> dow = new ArrayList<DayOfWeek>();
					String days = sesh[5];
					if(days.indexOf("1") != -1)
						dow.add(DayOfWeek.MONDAY);
					if(days.indexOf("2") != -1)
						dow.add(DayOfWeek.TUESDAY);
					if(days.indexOf("3") != -1)
						dow.add(DayOfWeek.WEDNESDAY);
					if(days.indexOf("4") != -1)
						dow.add(DayOfWeek.THURSDAY);
					if(days.indexOf("5") != -1)
						dow.add(DayOfWeek.FRIDAY);
					if(days.indexOf("6") != -1)
						dow.add(DayOfWeek.SATURDAY);
					if(days.indexOf("7") != -1)
						dow.add(DayOfWeek.SUNDAY);
			
					
					deb = sdf.parse(sesh[1]);
					fin = sdf.parse(sesh[2]);
					timeValue = new Time(formatter.parse(sesh[3]).getTime());
					maxi = Integer.parseInt(sesh[5]);
					double cost = Double.parseDouble(sesh[6]);

					Service serv = new Service(sesh[0],LocalDateTime.now(), deb, fin, timeValue, dow, maxi, ""+result,cost,sesh[7]);
					Validation.writeService(serv, CrudChoice.ADD);
					return true;
					
				}
				
				catch (Exception e) 
				{}
			}
			
		}
		
		System.out.println(CONVOINVALIDINPUT);
		response = sc.next();
		if(response.equals("0"))
			newService();
		
		return false;
	}

	/**
	 * Method that accepts the code of the pro and displays info 
	 * related to the services of that pro
	 * @return boolean If the service was successfully created
	 */
	public static boolean consult()
	{
		System.out.println(CONVOPROENTER);
		String response = sc.next();
		int result = intParse(response);

		ArrayList<Client> listdepro = Validation.getAllPros();
		boolean found = false;

		for(int i=0;i<listdepro.size();i++)
		{
			if(listdepro.get(i).getId().equals(""+result))
			{
				found = true;
				break;
			}
		}
		
		if(found)
		{
			ArrayList<Service> servs = Validation.getAllServices();
			
			for(int i=0;i<servs.size();i++)
			{
				Service s = servs.get(i);
				if(s.getCodeDePro().equals(""+result))
				{
					String debut = s.getDebut().toString();
					debut = debut.replaceAll("00:00:00", "");
					
					String fin = s.getFin().toString();
					fin = fin.replaceAll("00:00:00", "");
					System.out.println(s.getCodeDePro() + " - " + debut + " - " + fin + " - " + s.getCodeService() + " - " + s.getCommentaire());
				}
			}
			return true;
		}
		

		System.out.println(CONVOINVALIDINPUT);
		response = sc.next();
		if(response.equals("0"))
			consult();
		
		return false;
	}

	/**
	 * Method that confirms a client's presence to a session of a service
	 * @return boolean If the service was successfully created
	 */
	public static boolean confirmPresence()
	{
		System.out.println(CONVOUSERENTER);
		String response = sc.next();
		int result = intParse(response);
		
		int res = Validation.checkClientNumber(result);


		//Validates user number
		if(res == 1)
		{
			ArrayList<Service> servs = Validation.getAllServices();

			//Displays all the services
			for(int i=0;i<servs.size();i++)
			{
				System.out.println(servs.get(i).getName() + " - " + servs.get(i).getCodeService());
			}

			System.out.println(CONVOSERVICEPRESENCE);
			String signupservice = sc.next();

			for(int i=0;i<servs.size();i++)
			{
				if(signupservice.equals(servs.get(i).getCodeService()))
				{
					String comments = "";
					System.out.println(CONVOPRESENCECONFIRM);
					response = sc.next();
					if(!(response.equals("0")))
						comments = response;
					Presence p = new Presence(LocalDateTime.now(), servs.get(i).getCodeDePro(), result+"", signupservice, comments);
					Validation.confirmPresence(p, CrudChoice.ADD);
					return true;
				}
			}
			
			System.out.println(CONVOINVALIDUSER);
			response = sc.next();
			if(response.equals("0"))
				confirmPresence();
			else
				start();
		}
		
		else if(res == -1)
		{
			System.out.println(CONVONOCASHWELCOME);
		}
		
		else
		{
			System.out.println(CONVOINVALIDINPUT);
			response = sc.next();
			if(response.equals("0"))
				confirmPresence();
		}
		
		return false;
	}

	/**
	 * Helper method to parse a String and return it's value as an integer
	 * @param input String to pars
	 * @return integer parser from String
	 */
	public static int intParse(String input)
	{
		int result = -1;
		try 
		{
			result = Integer.parseInt(input);
		}
		catch(NumberFormatException e)
		{
			
		}
		return result;
	}

	
	
}
