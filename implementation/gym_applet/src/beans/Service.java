package beans;

import java.sql.Time;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

/**
 * Service bean class
 */
public class Service {

	String name;
	LocalDateTime ldtNow;
	Date debut;
	Date fin;
	Time heurDeService;
	ArrayList<DayOfWeek> recurrence;
	int maxClients;
	String codeDePro;
	String codeService;
	double frais;
	String commentaire;
	
	/**
	 * Constructor 
	 * @param name
	 * @param ldtNow
	 * @param debut
	 * @param fin
	 * @param heurDeService
	 * @param recurrence
	 * @param maxClients
	 * @param codeDePro
	 * @param commentaire
	 */
	public Service(String name, LocalDateTime ldtNow, Date debut, Date fin, Time heurDeService, ArrayList<DayOfWeek> recurrence,
			int maxClients, String codeDePro, double frais, String commentaire) {
		super();
		this.name = name;
		this.ldtNow = ldtNow;
		this.debut = debut;
		this.fin = fin;
		this.heurDeService = heurDeService;
		this.recurrence = recurrence;
		this.maxClients = maxClients;
		this.codeDePro = codeDePro;
		this.commentaire = commentaire;
		this.frais = frais;
		
		
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((codeDePro == null) ? 0 : codeDePro.hashCode());
		result = prime * result + ((codeService == null) ? 0 : codeService.hashCode());
		result = prime * result + ((commentaire == null) ? 0 : commentaire.hashCode());
		result = prime * result + ((debut == null) ? 0 : debut.hashCode());
		result = prime * result + ((fin == null) ? 0 : fin.hashCode());
		result = prime * result + ((heurDeService == null) ? 0 : heurDeService.hashCode());
		result = prime * result + ((ldtNow == null) ? 0 : ldtNow.hashCode());
		result = prime * result + maxClients;
		result = prime * result + ((recurrence == null) ? 0 : recurrence.hashCode());
		result = Math.abs(result);
		codeService = (result+"");
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getLdtNow() {
		return ldtNow;
	}
	public void setLdtNow(LocalDateTime ldtNow) {
		this.ldtNow = ldtNow;
	}
	public Date getDebut() {
		return debut;
	}
	public void setDebut(Date debut) {
		this.debut = debut;
	}
	public Date getFin() {
		return fin;
	}
	public void setFin(Date fin) {
		this.fin = fin;
	}
	public Time getHeurDeService() {
		return heurDeService;
	}
	public void setHeurDeService(Time heurDeService) {
		this.heurDeService = heurDeService;
	}
	public ArrayList<DayOfWeek> getRecurrence() {
		return recurrence;
	}
	public void setRecurrence(ArrayList<DayOfWeek> recurrence) {
		this.recurrence = recurrence;
	}
	public int getMaxClients() {
		return maxClients;
	}
	public void setMaxClients(int maxClients) {
		this.maxClients = maxClients;
	}
	public String getCodeDePro() {
		return codeDePro;
	}
	public void setCodeDePro(String codeDePro) {
		this.codeDePro = codeDePro;
	}
	public String getCodeService() {
		return codeService;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	public double getFrais() {
		return frais;
	}
	public void setFrais(double frais) {
		this.frais = frais;
	}
	
}
