package beans;

import java.time.LocalDateTime;

/**
 * Presence bean class
 */
public class Presence {

	LocalDateTime ldtnow;
	String proId;
	String clientId;
	String codeService;
	String comments;
	
	/**
	 * Constructor
	 * @param ldtnow
	 * @param proId
	 * @param clientId
	 * @param codeService
	 * @param comments
	 */
	public Presence(LocalDateTime ldtnow, String proId, String clientId, String codeService, String comments) {
		super();
		this.ldtnow = ldtnow;
		this.proId = proId;
		this.clientId = clientId;
		this.codeService = codeService;
		this.comments = comments;
	}
	
	public LocalDateTime getLdtnow() {
		return ldtnow;
	}
	public void setLdtnow(LocalDateTime ldtnow) {
		this.ldtnow = ldtnow;
	}
	public String getProId() {
		return proId;
	}
	public void setProId(String proId) {
		this.proId = proId;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getCodeService() {
		return codeService;
	}
	public void setCodeService(String codeService) {
		this.codeService = codeService;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
}
