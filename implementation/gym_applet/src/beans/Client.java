package beans;

import java.util.Random;

/**
 * Client bean class
 */
public class Client 
{

	private String id;
    private String nom;
    private String prenom;
    private String cell;
    private String email;
    private String address;
    private boolean pro;
    private boolean suspendu;


    /**
     * Constructor 
     * @param nom
     * @param prenom
     * @param cell
     * @param email
     * @param address
     * @param pro
     * @param suspendu
     */
	public Client(String nom, String prenom, String cell, String email, String address, boolean pro, boolean suspendu) 
    {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.cell = cell;
		this.email = email;
		this.address = address;
		this.pro = pro;
		this.id = "";
		this.suspendu = suspendu;
		
		
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((cell == null) ? 0 : cell.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = Math.abs(result);
		id += result;
		
		if(id.length() > 9)
		{
			id = id.substring(0,9);
		}
		if(id.length() < 9)
		{
			Random rn = new Random();
			int answer = rn.nextInt(10) + 1;
			id += answer;
			id = id.substring(0,9);
		}
	}
    
    
    public boolean isPro() {
		return pro;
	}
	public void setPro(boolean pro) {
		this.pro = pro;
	}
	
	public String getId() 
	{
		return id;
	}
	public void setId(String id) 
	{
		this.id = id;
	}


	public String getNom() 
    {
		return nom;
	}
	public void setNom(String nom) 
	{
		this.nom = nom;
	}


	public String getPrenom() 
	{
		return prenom;
	}
	public void setPrenom(String prenom) 
	{
		this.prenom = prenom;
	}


	public String getCell() 
	{
		return cell;
	}
	public void setCell(String cell) 
	{
		this.cell = cell;
	}


	public String getEmail() 
	{
		return email;
	}
	public void setEmail(String email) 
	{
		this.email = email;
	}


	public String getAddress() 
	{
		return address;
	}
	public void setAddress(String address) 
	{
		this.address = address;
	}

	
	public boolean isSuspendu() {
		return suspendu;
	}
	public void setSuspendu(boolean suspendu) {
		this.suspendu = suspendu;
	}


	@Override
	public String toString() 
	{
		return id + "+" + nom + "+" + prenom + "+" + cell + "+" + email + "+" + address;
	}

}