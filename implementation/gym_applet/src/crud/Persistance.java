package crud;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import beans.Client;
import beans.Presence;
import beans.Service;

/**
 * Handles calls to *Crud classes
 */
public class Persistance {
	
	/**
	 * Method to write to the client file, accepts a Client object, a Choice (insert, edit or delete the object)
	 * and the name of the file to which to write to
	 * @param filename String file name
	 * @param c Client object
	 * @param choice Enum CrudChoice
	 */
    public static void writeClient(String filename, Client c, CrudChoice choice) {
    	
        File xmlFile = new File(filename);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try 
        {
        	
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            
            switch(choice)
            {
            	case ADD:
            		ClientCrud.addElement(doc, c);
            		break;
            	case EDIT:
            		break;
            	case DELETE:
            		ClientCrud.deleteElement(doc, c);
            		break;
            }
            
            //update attribute value
            //updateAttributeValue(doc);
            
            //update Element value
            //updateElementValue(doc);
            
            //write the updated document to file or console
            doc.getDocumentElement().normalize();
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            
            
            
            String directory = System.getProperty("user.dir");
            directory = directory.replace("\\", "/");
            directory += "/";
            directory += filename;
            
            StreamResult result =  new StreamResult(directory);
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);
            System.out.println("XML file updated successfully");
            
        } catch (SAXException | ParserConfigurationException | IOException | TransformerException e1) {
            e1.printStackTrace();
        }
    }
    
	/**
	 * Method to write to the service file, accepts a Service object, a Choice (insert, edit or delete the object)
	 * and the name of the file to which to write to
	 * @param filename String file name
	 * @param c Service object
	 * @param choice Enum CrudChoice
	 */
    public static void writeService(String filename, Service c, CrudChoice choice) {
    	
        File xmlFile = new File(filename);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try 
        {
        	
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            
            switch(choice)
            {
            	case ADD:
            		ServiceCrud.addElement(doc, c);
            		break;
            	case EDIT:
            		break;
            	case DELETE:
            		ServiceCrud.deleteElement(doc, c);
            		break;
            }
            
            //update attribute value
            //updateAttributeValue(doc);
            
            //update Element value
            //updateElementValue(doc);
            
            //write the updated document to file or console
            doc.getDocumentElement().normalize();
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            
            
            
            String directory = System.getProperty("user.dir");
            directory = directory.replace("\\", "/");
            directory += "/";
            directory += filename;
            
            StreamResult result =  new StreamResult(directory);
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);
            System.out.println("XML file updated successfully");
            
        } catch (SAXException | ParserConfigurationException | IOException | TransformerException e1) {
            e1.printStackTrace();
        }
    }  
    
	/**
	 * Method to write to the presence file, accepts a Presence object, a Choice (insert, edit or delete the object)
	 * and the name of the file to which to write to
	 * @param filename String file name
	 * @param c Presence object
	 * @param choice Enum CrudChoice
	 */
    public static void writePresence(String filename, Presence c, CrudChoice choice) 
    { 	
        File xmlFile = new File(filename);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try 
        {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            
            switch(choice)
            {
            	case ADD:
            		PresenceCrud.addElement(doc, c);
            		break;
            	case EDIT:
            		break;
            	case DELETE:
            		break;
            }
            
            //update attribute value
            //updateAttributeValue(doc);
            
            //update Element value
            //updateElementValue(doc);
            
            //write the updated document to file or console
            doc.getDocumentElement().normalize();
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            
            
            
            String directory = System.getProperty("user.dir");
            directory = directory.replace("\\", "/");
            directory += "/";
            directory += filename;
            
            StreamResult result =  new StreamResult(directory);
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);
            System.out.println("XML file updated successfully");
            
        } catch (SAXException | ParserConfigurationException | IOException | TransformerException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Registers a client to a service
     * 
     * @param filename String file name
	 * @param choice Enum CrudChoice
     * @param serv Service to register to
     * @param clientCode Client to register
     */
    public static void writeInscription(String filename, CrudChoice choice, Service serv, String clientCode) 
    { 	
        File xmlFile = new File(filename);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try 
        {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            
            switch(choice)
            {
            	case ADD:
            		InscriptionCrud.addElement(doc, serv, clientCode);
            		break;
            	case EDIT:
            		break;
            	case DELETE:
            		break;
            }
            
            //write the updated document to file or console
            doc.getDocumentElement().normalize();
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            
            
            
            String directory = System.getProperty("user.dir");
            directory = directory.replace("\\", "/");
            directory += "/";
            directory += filename;
            
            StreamResult result =  new StreamResult(directory);
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);
            System.out.println("XML file updated successfully");
            
        } catch (SAXException | ParserConfigurationException | IOException | TransformerException e1) {
            e1.printStackTrace();
        }
    }
    
    /**
     * Create a TEF file
     * @param filename Filename
     * @param als info to save to the file
     */
    public static void createTef(String filename, ArrayList<String> als) 
    { 	
    	ArrayList<String> alz = new ArrayList<String>();
    	for(int f=0;f<als.size();f++)
    	{
    		if(!als.get(f).trim().equals(""))
    		{
    			alz.add(als.get(f));
    		}
    	}
    	als = alz;
    	
    	
    	filename = filename+".xml";
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(filename, "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		writer.close();
		
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
        Document doc = dBuilder.newDocument();
        Element root = doc.createElement("info");
        doc.appendChild(root);
        doc.getDocumentElement().normalize();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		}
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(filename));
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	
    	
		NodeList employees = doc.getElementsByTagName("info");
        Element emp = null;
        for(int i=0; i<employees.getLength();i++)
        {
            emp = (Element) employees.item(i);
            
            for(int j = 0; j < als.size(); j++)
            {
	            Element elo = doc.createElement("i");
	            elo.appendChild(doc.createTextNode(als.get(j)));
	            emp.appendChild(elo);
            }
            
        }
        
		try
		{
	        doc.getDocumentElement().normalize();
	        transformerFactory = TransformerFactory.newInstance();
	        transformer = transformerFactory.newTransformer();
	        source = new DOMSource(doc);
	        
	        
	        
	        String directory = System.getProperty("user.dir");
	        directory = directory.replace("\\", "/");
	        directory += "/";
	        directory += filename;
	        
	        result =  new StreamResult(directory);
	        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        transformer.transform(source, result);
	        System.out.println("XML file updated successfully");
	    } catch (TransformerException e1) {
	        e1.printStackTrace();
	    }
 
    }
    
    /**
     * Helper method to delete a file
     * @param filename String file name
     */
	public static void wipeFile(String filename)
	{
		File file = new File(filename);
        file.delete();
	}





}