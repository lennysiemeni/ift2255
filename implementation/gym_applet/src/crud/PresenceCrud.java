package crud;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import beans.Presence;

/**
 * Class that handles all functionality that deals with Presence object.
 */
public class PresenceCrud {

	/**
	 * Method that accepts a Presence object and saves it to an XML file
	 * @param doc Document to be saved to
	 * @param c Presence object
	 */
    static void addElement(Document doc, Presence c) {
        NodeList employees = doc.getElementsByTagName("Presences");
        Element emp = null;
        
        //loop for each employee
        for(int i=0; i<employees.getLength();i++){
            emp = (Element) employees.item(i);
            
            Element elo = doc.createElement("prez");
            
            Element ldt = doc.createElement("ldt");
            Element proId = doc.createElement("proId");
            Element clientId = doc.createElement("clientId");
            Element codeService = doc.createElement("codeService");
            Element comments = doc.createElement("comments");
            
            ldt.appendChild(doc.createTextNode(c.getLdtnow().toString()));
            proId.appendChild(doc.createTextNode(c.getProId()));
            clientId.appendChild(doc.createTextNode(c.getClientId()));
            codeService.appendChild(doc.createTextNode(c.getCodeService()));
            comments.appendChild(doc.createTextNode(c.getComments()));
            
            elo.appendChild(ldt);
            elo.appendChild(proId);
            elo.appendChild(clientId);
            elo.appendChild(codeService);
            elo.appendChild(comments);
            
            emp.appendChild(elo);
        }
    }
	
}
