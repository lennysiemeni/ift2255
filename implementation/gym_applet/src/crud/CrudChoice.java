package crud;

/**
 * CrudChoice Enum: Much of the code to access a file is similar, 
 * so we centralized Insert, Edit and Delete operations in the same method that
 * accepts a CrudChoice that decides whether the method is being called for Inserting, editing, or deleting
 */
public enum CrudChoice {
    ADD,
    EDIT,
    DELETE;
}