package crud;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import beans.Client;

/**
 * Class that handles all functionality that deals with Client objects, saving, deleting, reading.
 */
public class ClientCrud {

	/**
	 * Method that accepts a Client object and saves it to an XML file
	 * @param doc Document to be saved to
	 * @param c Client object
	 */
    static void addElement(Document doc, Client c) {
        NodeList employees = doc.getElementsByTagName("Employees");
        Element emp = null;
        
        //loop for each employee
        for(int i=0; i<employees.getLength();i++){
            emp = (Element) employees.item(i);
            
            Element elo = doc.createElement("client");
            Element nom = doc.createElement("nom");
            Element prenom = doc.createElement("prenom");
            Element cell = doc.createElement("cell");
            Element email = doc.createElement("email");
            Element address = doc.createElement("address");
            Element pro = doc.createElement("pro");
            Element sus = doc.createElement("sus");
            

            nom.appendChild(doc.createTextNode(c.getNom()));
            prenom.appendChild(doc.createTextNode(c.getPrenom()));
            cell.appendChild(doc.createTextNode(c.getCell()));
            email.appendChild(doc.createTextNode(c.getEmail()));
            address.appendChild(doc.createTextNode(c.getAddress()));
            pro.appendChild(doc.createTextNode(c.isPro()+""));
            sus.appendChild(doc.createTextNode(c.isSuspendu()+""));
            

            elo.appendChild(nom);
            elo.appendChild(prenom);
            elo.appendChild(cell);
            elo.appendChild(email);
            elo.appendChild(address);
            elo.appendChild(pro);
            elo.appendChild(sus);
            elo.setAttribute("id", c.getId());
            emp.appendChild(elo);
        }
    }

    /**
     * Method to delete a Client object
     * @param doc Document to delete from
     * @param c Client object to delete
     */
    static void deleteElement(Document doc, Client c) {
        NodeList employees = doc.getElementsByTagName("client");
        Element emp = null;
        for(int i=0; i<employees.getLength();i++)
        {
        	emp = (Element)employees.item(i);
        	if(emp.getAttribute("id").equals(c.getId()))
        	{
        		emp.getParentNode().removeChild(emp);
        	}
        }
    }
    
    /**
     * Reads the XML file that holds all clients, builds up the Client objects
     * adds them to an ArrayList and returns the ArrayList
     * @param filename String file name to read from
     * @return ArrayList that holds all Client objects
     */
    public static ArrayList<Client> reader(String filename)
    {
    	ArrayList<Client> allClients = new ArrayList<Client>();
    	Client current = null;
        try {

        	File fXmlFile = new File(filename);
        	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        	Document doc = dBuilder.parse(fXmlFile);
        			
        	doc.getDocumentElement().normalize();

        	NodeList nList = doc.getElementsByTagName("client");
        			
        	for (int temp = 0; temp < nList.getLength(); temp++) {

        		Node nNode = nList.item(temp);
        				
        		if (nNode.getNodeType() == Node.ELEMENT_NODE) {

        			Element eElement = (Element) nNode;
        			String pr = eElement.getElementsByTagName("pro").item(0).getTextContent();
        			String sus = eElement.getElementsByTagName("sus").item(0).getTextContent();
        			boolean b1 = Boolean.parseBoolean(pr);
        			boolean b2 = Boolean.parseBoolean(sus);
        			current  = new Client(eElement.getElementsByTagName("nom").item(0).getTextContent(),
        					eElement.getElementsByTagName("prenom").item(0).getTextContent(),
        					eElement.getElementsByTagName("cell").item(0).getTextContent(),
        					eElement.getElementsByTagName("email").item(0).getTextContent(),
        					eElement.getElementsByTagName("address").item(0).getTextContent(), b1, b2);
        			allClients.add(current);
        		}
        	}
            } catch (Exception e) {
        	e.printStackTrace();
            }
        
        return allClients;
    }
	
}
