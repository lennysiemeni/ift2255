package crud;

import java.io.File;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import beans.Service;

public class ServiceCrud {
	
	/**
	 * Method that accepts a Service object and saves it to an XML file
	 * @param doc Document to be saved to
	 * @param c Service object
	 */
	 static void addElement(Document doc, Service c) {
	        NodeList employees = doc.getElementsByTagName("Services");
	        Element emp = null;
	        
	        //loop for each employee
	        for(int i=0; i<employees.getLength();i++){
	            emp = (Element) employees.item(i);
	            
	            Element elo = doc.createElement("service");
	            Element name = doc.createElement("name");
	            Element ldtnow = doc.createElement("ldtnow");
	            Element debut = doc.createElement("debut");
	            Element fin = doc.createElement("fin");
	            Element hds = doc.createElement("heurDeService");
	            Element rec = doc.createElement("recurrence");
	            Element maxi = doc.createElement("maxClients");
	            Element codePro = doc.createElement("codeDePro");
	            Element comments = doc.createElement("comments");
	            Element cost = doc.createElement("cost");

	            
	            name.appendChild(doc.createTextNode(c.getName()));
	            DateTimeFormatter formattar = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	            String formatDateTime = c.getLdtNow().format(formattar);
	            ldtnow.appendChild(doc.createTextNode(formatDateTime));
	            debut.appendChild(doc.createTextNode(c.getDebut().toString()));
	            fin.appendChild(doc.createTextNode(c.getFin().toString()));
	            hds.appendChild(doc.createTextNode(c.getHeurDeService().toString()));
	            rec.appendChild(doc.createTextNode(c.getRecurrence().toString())); 
	            maxi.appendChild(doc.createTextNode(c.getMaxClients()+""));
	            codePro.appendChild(doc.createTextNode(c.getCodeDePro()));
	            comments.appendChild(doc.createTextNode(c.getCommentaire()));
	            cost.appendChild(doc.createTextNode(c.getFrais()+""));
	            
	            elo.appendChild(name);
	            elo.appendChild(ldtnow);
	            elo.appendChild(debut);
	            elo.appendChild(fin);
	            elo.appendChild(hds);
	            elo.appendChild(rec);
	            elo.appendChild(maxi);
	            elo.appendChild(codePro);
	            elo.appendChild(cost);
	            elo.appendChild(comments);
	            elo.setAttribute("id", c.getCodeService());
	            emp.appendChild(elo);
	        }
	    }

	 /**
	  * Deletes services
	  * 
	  * @param doc Document to handle
	  * @param c Service to delete
	  */
	    static void deleteElement(Document doc, Service c) 
	    {
	        NodeList employees = doc.getElementsByTagName("service");
	        Element emp = null;
	        for(int i=0; i<employees.getLength();i++)
	        {
	        	emp = (Element)employees.item(i);
	        	if(emp.getAttribute("id").equals(c.getCodeService()))
	        	{
	        		emp.getParentNode().removeChild(emp);
	        	}
	        }
	    }
	    
	    /**
	     * Reads the XML file that holds all services, builds up the Service objects
	     * adds them to an ArrayList and returns the ArrayList
	     * 
	     * @param filename String file name to read from
	     * @return ArrayList that holds all Service objects
	     */
	    public static ArrayList<Service> reader(String filename)
	    {
	    	ArrayList<Service> allClients = new ArrayList<Service>();
	        try 
	        {
	        	File fXmlFile = new File(filename);
	        	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	        	Document doc = dBuilder.parse(fXmlFile);
	        	doc.getDocumentElement().normalize();
	
	        	NodeList nList = doc.getElementsByTagName("service");
	        	Service service;		
	        	for (int temp = 0; temp < nList.getLength(); temp++) 
	        	{
	        		Node nNode = nList.item(temp);
	        		if (nNode.getNodeType() == Node.ELEMENT_NODE) 
	        		{
	        			Element eElement = (Element) nNode;
	        			
	        			String str = eElement.getElementsByTagName("ldtnow").item(0).getTextContent();
	        			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	        			LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
	        			DateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
	        			SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", new Locale("us"));
	        			
	        			Date deb = sdf.parse(eElement.getElementsByTagName("debut").item(0).getTextContent());
	        			Date fin = sdf.parse(eElement.getElementsByTagName("fin").item(0).getTextContent());
	        			Time t = new Time(timeFormatter.parse(eElement.getElementsByTagName("heurDeService").item(0).getTextContent()).getTime());
	        			
	        			ArrayList<DayOfWeek> dow = new ArrayList<DayOfWeek>();
	        			String days = eElement.getElementsByTagName("recurrence").item(0).getTextContent();
	        			days = days.replaceAll("\\[", "");
	        			days = days.replaceAll("\\]", "");
	        			String[] dds = days.split(",");
	        			
	        			String field = "";
	        			for(int i=0;i<dds.length;i++)
	        			{
	        				field = dds[i].trim();
	        				switch(field)
	        				{
	        					case "MONDAY":
	        						dow.add(DayOfWeek.MONDAY);
	        						break;
	        					case "TUESDAY":
	        						dow.add(DayOfWeek.TUESDAY);
	        						break;
	        					case "WEDNESDAY":
	        						dow.add(DayOfWeek.WEDNESDAY);
	        						break;
	        					case "THURSDAY":
	        						dow.add(DayOfWeek.THURSDAY);
	        						break;
	        					case "FRIDAY":
	        						dow.add(DayOfWeek.FRIDAY);
	        						break;
	        					case "SATURDAY":
	        						dow.add(DayOfWeek.SATURDAY);
	        						break;
	        					case "SUNDAY":
	        						dow.add(DayOfWeek.SUNDAY);
	        						break;
	        				}
	        			}
	        			
	        			String maxi = eElement.getElementsByTagName("maxClients").item(0).getTextContent();
	        			int maximum = Integer.parseInt(maxi);
	        			double cost = Double.parseDouble(eElement.getElementsByTagName("cost").item(0).getTextContent());
	        			
	        			service = new Service(eElement.getElementsByTagName("name").item(0).getTextContent(),
	        					dateTime,deb,fin,t,dow,maximum,
	        					eElement.getElementsByTagName("codeDePro").item(0).getTextContent(), cost,
	        					eElement.getElementsByTagName("comments").item(0).getTextContent());
	        			
	        			allClients.add(service);
	        		}
	        	}
	            } 
	        
	        	catch (Exception e) {
	        	e.printStackTrace();
	            }
	        
	        return allClients;

	    }
}
