package crud;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import beans.Service;
import system_comms.Validation;

/**
 * Class that handles all functionality that deals with Inscription
 */
public class InscriptionCrud {

    public static void addElement(Document doc, Service serv, String clientId) {
        NodeList insc = doc.getElementsByTagName("insc");
        Element emp = null;
        String serviceCode = "";
        
        
        String threeLetterServiceCode = serv.getCodeService().substring(0, 3);
        serviceCode += threeLetterServiceCode;
        
        String seanceCount = "00";
        String currentBigCount = "";
        int scInt;
        int cbcInt;
        ArrayList<String[]> lol = reader(Validation.INSCRIPTIONFILE);
        for(int i = 0; i < lol.size(); i++)
        {
        	if(lol.get(i)[0].contains(threeLetterServiceCode))
        	{
        		currentBigCount = lol.get(i)[0].substring(3, 5);
        		scInt = Integer.parseInt(seanceCount);
        		cbcInt = Integer.parseInt(currentBigCount);
        		if(cbcInt > scInt)
        		{
        			seanceCount = currentBigCount;
        		}
        	}
        }
        //Increment seanceCount
        scInt = Integer.parseInt(seanceCount);
        scInt++;
        seanceCount = scInt+"";
        
        serviceCode += seanceCount;
        
        serviceCode += serv.getCodeDePro().substring(0, 2);
        
        for(int i=0; i<insc.getLength();i++){
            emp = (Element) insc.item(i);
            
            Element elo = doc.createElement("in");
            Element nom = doc.createElement("client");
            Element prenom = doc.createElement("service");

            nom.appendChild(doc.createTextNode(clientId+""));
            prenom.appendChild(doc.createTextNode(serviceCode));
            

            elo.appendChild(nom);
            elo.appendChild(prenom);
            emp.appendChild(elo);
        }
    }
    
    public static ArrayList<String[]> reader(String filename)
    {
    	ArrayList<String[]> rowList = new ArrayList<String[]>();
        try {

        	File fXmlFile = new File(filename);
        	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        	Document doc = dBuilder.parse(fXmlFile);
        			
        	doc.getDocumentElement().normalize();

        	NodeList nList = doc.getElementsByTagName("in");
        			
        	for (int temp = 0; temp < nList.getLength(); temp++) 
        	{

        		Node nNode = nList.item(temp);
        				
        		if (nNode.getNodeType() == Node.ELEMENT_NODE) 
        		{
        			Element eElement = (Element) nNode;
        			String client = eElement.getElementsByTagName("client").item(0).getTextContent();
        			String service = eElement.getElementsByTagName("service").item(0).getTextContent();

        			rowList.add(new String[] { service, client });
        		}
        	}
            } catch (Exception e) {
        	e.printStackTrace();
            }
        
       return rowList;
    }
	
}
