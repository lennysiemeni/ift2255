package crud;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class that handles the reading of a TEF file.
 *
 */
public class TefRead {
	/**
	 * Reads a TEF file given it's filename
	 * @param filename Filename
	 * @return ArrayList<String> containing all the information from the file
	 */
	public static ArrayList<String> readTefFile(String filename) 
	{
		filename = filename+".xml";
		ArrayList<String> als = new ArrayList<String>();
		try 
		{
        	File fXmlFile = new File(filename);
        	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        	Document doc = dBuilder.parse(fXmlFile);
        	doc.getDocumentElement().normalize();

        	NodeList nList = doc.getElementsByTagName("i");
        	for (int temp = 0; temp < nList.getLength(); temp++) 
        	{
        		Node nNode = nList.item(temp);	
        		if (nNode.getNodeType() == Node.ELEMENT_NODE) 
        		{
        			Element eElement = (Element) nNode;
        			String data = eElement.getTextContent();
        			als.add(data);
        		}
        	}
        	
		}
		
		catch (Exception e) 
		{
        	e.printStackTrace();
        }
		return als;
	}
}
