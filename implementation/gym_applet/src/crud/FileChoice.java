package crud;

/**
 *FileChoice Enum: Enum that has the values of Client, Service, or Pro
 */
public enum FileChoice {
    CLIENT,
    SERVICE,
    PRO;
}
